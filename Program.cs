﻿#nullable enable

using Newtonsoft.Json;
using System;
using System.CommandLine;
using System.CommandLine.Builder;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

const int DEFAULT_BUFFER_SIZE = 4096;

[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
static async Task<int> MinifyOrPrettify(TextReader inputReader, TextWriter outputWriter, Formatting formatting, CancellationToken cancellationToken = default)
{
    try
    {
        using var jsonInput = new JsonTextReader(inputReader) { MaxDepth = null };
        using var jsonOutput = new JsonTextWriter(outputWriter) { AutoCompleteOnClose = true, Formatting = formatting };

        await jsonOutput.WriteTokenAsync(jsonInput, cancellationToken);
    }
    catch (JsonException e)
    {
        await Console.Error.WriteLineAsync($@"There was an error while converting JSON.
[{e.GetType().Name}]: {e.Message}".AsMemory(), cancellationToken);

        return -1;
    }

    return 0;
}

[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
static async Task<int> SanitizeAndInitialize(bool yes, bool no, string? encodingInput, string encodingOutput, FileInfo? input, FileInfo? output, Formatting formatting, CancellationToken cancellationToken = default)
{
    Encoding? inputEncoding = null;

    if (!string.IsNullOrWhiteSpace(encodingInput))
    {
        try
        {
            inputEncoding = Encoding.GetEncoding(encodingInput!);
        }
        catch (ArgumentException)
        {
            await Console.Error.WriteLineAsync($@"""{encodingInput!}"" is not a supported input file enconding.".AsMemory(), cancellationToken);

            return -1;
        }
    }

    Encoding outputEncoding;

    try
    {
        outputEncoding = Encoding.GetEncoding(encodingOutput!);
    }
    catch (ArgumentException)
    {
        await Console.Error.WriteLineAsync($@"""{encodingOutput!}"" is not a supported output file encoding.".AsMemory(), cancellationToken);

        return -1;
    }

    if (input?.Exists is false)
    {
        await Console.Error.WriteLineAsync($@"The input file ""{ input.FullName }"" does not exist. Aborting.".AsMemory(), cancellationToken);

        return -1;
    }

    if (output?.Exists is true)
    {
        if (await AcquireUserConfirmation(
            $@"The file ""{ output!.FullName }"" already exists. Should it be overwritten?", 
            false, 
            no ? false : yes ? true : null, 
            cancellationToken
        ) is false)
        {
            return 0;
        }
    }

    try
    {
        await using var inputStream = input is { FullName: var inputFile }
            ? new FileStream(inputFile, FileMode.Open, FileAccess.Read)
            : Console.OpenStandardInput();

        await using var inputBuffer = new BufferedStream(inputStream, DEFAULT_BUFFER_SIZE);

        using var inputReader = inputEncoding is not null
            ? new StreamReader(inputStream, inputEncoding!)
            : new StreamReader(inputStream);

        await using var outputStream = output is { FullName: var outputFile }
            ? new FileStream(outputFile, FileMode.Create, FileAccess.Write)
            : Console.OpenStandardOutput();

        await using var outputBuffer = new BufferedStream(outputStream, DEFAULT_BUFFER_SIZE);

        using var outputWriter = new StreamWriter(outputStream, outputEncoding!);

        return await MinifyOrPrettify(inputReader, outputWriter, formatting, cancellationToken);
    }
    catch (Exception e)
    {
        await Console.Error.WriteLineAsync($@"There was an I/O error while performing file operations.
[{e.GetType().Name}]: {e.Message}".AsMemory(), cancellationToken);

        return -1;
    }
}

[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
static async Task<int> ListEncodings(CancellationToken cancellationToken = default)
{
    foreach (var encoding in Encoding.GetEncodings())
    {
        if (encoding?.GetEncoding() is { WebName: var name } && name is not null)
        {
            await Console.Out.WriteLineAsync(name!.AsMemory(), cancellationToken);
        }
    }

    return 0;
}

[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
static async Task<bool> AcquireUserConfirmation(string question, bool? defaultAnswer = default, bool? maybeAnswer = default, CancellationToken cancellationToken = default)
{
    while (maybeAnswer is not bool)
    {
        await Console.Out.WriteAsync($"{question} [{defaultAnswer switch { true => "Y/n", false => "y/N", _ => "y/n" }}]: ".AsMemory(), cancellationToken);

        maybeAnswer = (await Console.In.ReadLineAsync())?.ToLowerInvariant() switch
        {
            "y" or "yes"                                => true,
            "n" or "no"                                 => false,
            var str when string.IsNullOrWhiteSpace(str) => defaultAnswer,
            _                                           => null
        };
    }

    return maybeAnswer!.Value;
}

[MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
static Parser CreateCommandLineParser()
{
    var optYes = new Option<bool>(
        aliases:         new[] { "-y", "--yes", "--yes-to-all" },
        description:     @"All yes/no-based user confirmations (i.e. when an exisiting file would be overwritten by an operation) will be answered YES automatically."
    );

    var optNo = new Option<bool>(
        aliases:         new[] { "-n", "--no", "--no-to-all" },
        description:     @"All yes/no-based user confirmations (i.e. when an exisiting file would be overwritten by an operation) will be answered NO automatically.
This takes advantage over ""--yes""."
    );

    var optInputEncoding = new Option<string?>(
        aliases:         new[] { "-i", "--encoding-input" },
        getDefaultValue: static () => null,
        description:     @"Specifies the input file encoding.
If this options is not specified or left empty, the input file encoding is detected automatically."
    );

    var optOutputEncoding = new Option<string>(
        aliases:         new[] { "-o", "--encoding-output" },
        getDefaultValue: static () => "utf-8",
        description:     @"Specifies the output file encoding."
    );

    var argInput = new Argument<FileInfo?>(
        name:            "input",
        getDefaultValue: static () => null,
        description:     @"The input JSON file path.
If this argument is not specified or left empty, the input defaults to STDIN."
    );

    var argOutput = new Argument<FileInfo?>(
        name:            "output",
        getDefaultValue: static () => null,
        description:     @"The output JSON file path.
If this argument is not specified or left empty, the output defaults to STDOUT."
    );

    return new CommandLineBuilder()
        .UseDefaults()
        .AddCommand(
            new CommandBuilder(
                new Command("minify", @"Minifies the input JSON file and writes the result to the specified output.")
                {
                    Handler = CommandHandler.Create(static (InvocationContext invocationContext, bool yes, bool no, string? encodingInput, string encodingOutput, FileInfo? input, FileInfo? output)
                        => SanitizeAndInitialize(yes, no, encodingInput, encodingOutput, input, output, Formatting.None, invocationContext.GetCancellationToken())
                    )
                }
            )
            .AddOption(optYes)
            .AddOption(optNo)
            .AddOption(optInputEncoding)
            .AddOption(optOutputEncoding)
            .AddArgument(argInput)
            .AddArgument(argOutput)
            .Command
        )
        .AddCommand(
            new CommandBuilder(
                new Command("prettify", @"Prettifies the input JSON file (""pretty print"") and writes the result to the specified output.")
                {
                    Handler = CommandHandler.Create(static (InvocationContext invocationContext, bool yes, bool no, string? encodingInput, string encodingOutput, FileInfo? input, FileInfo? output)
                        => SanitizeAndInitialize(yes, no, encodingInput, encodingOutput, input, output, Formatting.Indented, invocationContext.GetCancellationToken())
                    )
                }
            )
            .AddOption(optYes)
            .AddOption(optNo)
            .AddOption(optInputEncoding)
            .AddOption(optOutputEncoding)
            .AddArgument(argInput)
            .AddArgument(argOutput)
            .Command
        )
        .AddCommand(
            new Command("encodings", @"Lists all supported file encodings.")
            {
                Handler = CommandHandler.Create(static (InvocationContext invocationContext)
                    => ListEncodings(invocationContext.GetCancellationToken())
                )
            }
        )
        .Build();
}

return await CreateCommandLineParser().InvokeAsync(args);